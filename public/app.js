//const modelURL = 'Felladrin/onnx-Pythia-31M-Chat-v1';
//const modelURL = 'Felladrin/onnx-Minueza-32M-UltraChat';
//const modelURL = 'Felladrin/onnx-TinyMistral-248M-Chat-v2';
const modelURL = 'Xenova/Qwen1.5-0.5B-Chat';


const currentDownloads = {}

var currentMessage = null; //this will hold the messages by AI to update them while answering

var messageParameters = {
    max_new_tokens: 64,
    do_sample: true,
    //temperature: 0.3,
    // top_k: 35,
    // top_p: 0.55,
    temperature: 0.9,
    repetition_penalty: 1.1,
    top_p: 0.8
}

const currentChat = [
    {
        role: "system",
        content: "You are a highly knowledgeable and friendly assistant. Your goal is to understand and respond to user inquiries with clarity. Your interactions are always respectful, helpful, and focused on delivering the most accurate information to the user.",
    },
    {
        role: "user",
        content: "Hey! Got a question for you!",
    },
    {
        role: "assistant",
        content: "Sure! What's it?",
    },
]

document.addEventListener("DOMContentLoaded", () => {

    const sendButton = document.getElementById('send-button');
    const chatInput = document.getElementById('chat-input');
    const chatMessages = document.getElementById('chat-messages');
    

    const disableUI = () => {
        sendButton.setAttribute('disabled', true);
        sendButton.innerText = 'Answering...'
    }

    const enableUI = () => {
        sendButton.removeAttribute('disabled');
        sendButton.innerText = 'Send'
    }


    var aiWorker = new Worker('worker.js', {
        type: "module"
    });    

    const chat = (text) => {
        const newUserMessage = {
            role: 'user',
            content: text
        }
        currentChat.push(newUserMessage);

        aiWorker.postMessage({
            action: 'chat',
            chat: currentChat,
            parameters: messageParameters
        })
    }

    const download = (model) => {
        aiWorker.postMessage({
            action: 'download',
            task: 'text-generation',
            model: model
        });
    }

    const addMessage = (message, role) => {
        const newMessageElement = document.createElement('div');
        newMessageElement.classList.add("chat-message");
        newMessageElement.classList.add(role);

        newMessageElement.innerHTML = message;
        chatMessages.appendChild(newMessageElement);
        chatMessages.scrollTop = chatMessages.scrollHeight;
        return newMessageElement;
    }

    aiWorker.addEventListener('message', (event) => {
        const { status, result } = event.data;

        if (status == 'downloading') {

            if (result.status == 'progress') {
                currentDownloads[result.file] = result.progress;
                var globalProgress = 0;
                for (let file in currentDownloads) {
                    globalProgress += currentDownloads[file];
                }
                globalProgress /= Object.keys(currentDownloads).length;

                document.getElementById('downloading-message').innerText = `Downloading model... ${Math.floor(globalProgress)}%`;
            }
        } else if (status == 'update') {
            if (currentMessage == null) {
                currentMessage = addMessage(result, 'assistant');
            } else {
                currentMessage.innerHTML = result;
            }

            chatMessages.scrollTop = chatMessages.scrollHeight;

        } else if (status == 'result') {
            currentMessage = null;
            const newAssistantMessage = {
                role: 'assistant',
                content: result
            }
            currentChat.push(newAssistantMessage);
            enableUI();

        } else if (status == 'ready') {
            addMessage(`<small>Model ready! More information here <a href="https://huggingface.co/${modelURL}" target="_blank">${modelURL}</a></small>`, 'system');

            const sendMessage = () => {
                disableUI();
                addMessage(chatInput.value, 'user');
                chat(chatInput.value);
                chatInput.value = '';
            }

            sendButton.addEventListener('click', sendMessage);
            chatInput.addEventListener('keypress', (event) => {
                if (event.key === 'Enter') {
                    sendMessage();
                }
            });
            enableUI();
        }
    })

    download(modelURL);
    addMessage(`<small id="downloading-message">Downloading model...</small>`, 'system');
})