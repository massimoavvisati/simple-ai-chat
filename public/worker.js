
import {
    pipeline,
    env
} from "https://cdn.jsdelivr.net/npm/@xenova/transformers@2.17.1";

env.allowLocalModels = false; //this need to be set if not working with local models

var generator, task, model;

const progressCallback = (data) => {
    self.postMessage({
        status: 'downloading',
        result: data
    });
}

const getLastAssistantOutput = (text) => {
    const lastAssistantIndex = text.lastIndexOf('assistant\n');
    if (lastAssistantIndex !== -1) {
        return text.substr(lastAssistantIndex + 'assistant\n'.length);
    }
    return "";
}

const updateCallback = (beams) => {
    const decodedText = generator.tokenizer.decode(beams[0].output_token_ids, {
        skip_special_tokens: true,
    })

    const conversation = decodedText;
    const start = conversation.lastIndexOf("assistant\n");
    const lastMessage = conversation
        .substr(start)
        .replace("assistant\n", "");

    self.postMessage({
        status: 'update',
        result: lastMessage
    });
}

const resultCallback = (output) => {
    self.postMessage({
        status: 'result',
        result: output
    })
}

self.addEventListener('message', async (event) => {
    const message = event.data;

    if (message.action == 'download') {
        task = message.task;
        model = message.model;

        generator = await pipeline(task, model, {
            progress_callback: progressCallback
        });

        self.postMessage({
            status: 'ready',
            task: task,
            model: model
        });

    } else if (message.action == 'chat') {
        // We alwasys receive all messages
        const messages = message.chat;
        const parameters = message.parameters ? message.parameters : {}

        // Apply chat template
        const text = generator.tokenizer.apply_chat_template(messages, {
            tokenize: false,
            add_generation_prompt: true,
        });

        const output = await generator(text,
            {
                ...message.generation,
                callback_function: updateCallback,
                max_new_tokens: parameters.max_new_tokens != null ? parameters.max_new_tokens : 32,
                do_sample: parameters.do_sample != null ? parameters.do_sample : true,
                temperature: parameters.temperature,
                top_p: parameters.top_p,
                top_k: parameters.top_k,
                penalty_alpha: parameters.penalty_alpha,
                num_beams: parameters.num_beams != null ? parameters.num_beams : 1,
                repetition_penalty: parameters.repetition_penalty,
                temperature: parameters.temperature,
                early_stopping: parameters.early_stopping,
                return_full_text: parameters.return_full_text
            }
        );

        const conversation = output[0].generated_text;
        const start = conversation.lastIndexOf("assistant\n");
        const lastMessage = conversation
            .substr(start)
            .replace("assistant\n", "");

        resultCallback(lastMessage);

    }
});